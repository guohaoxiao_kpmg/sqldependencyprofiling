import re
import sys
class Tree(object):
    "Generic tree node."
    def __init__(self, name='root', children=None):
        self.name = name
        self.children = []
        if children is not None:
            for child in children:
                self.add_child(child)
    def __str__(self, level=0):
       
        ret = "\t"*(level)+repr(self.name)+"\n"
        for child in self.children:
            ret += child.__str__(level+1)
        return ret

    def __repr__(self):
        return '<tree node representation>'
    def add_child(self, node):
        assert isinstance(node, Tree)
        self.children.append(node)
    def hasChild(self,leaf):
        for child in self.children:
            if child.name == leaf:
                return True
        return False




nodeSet=set()
rootSet={}


f = open(sys.argv[1], 'r')
content =" ".join([line for line in f.readlines() if "--" not in line]).replace("\n"," ").lower()

for content in content.split(";"):
    p = re.compile('\s*create\s*(external|temporary)?\s*table\s*([_|\w]*\.?[_|\w]+).*select')
    if len(p.findall(content))==0:
       continue
    name=p.findall(content)[0][1]
    tree=Tree(name,None)
    p = re.compile('\s*(from|join) \s*([_|\w]*\.?[_|\w]+)')
    for tuple in p.findall(content):
        leaf=tuple[1]
        if rootSet.has_key(leaf) is False:
            leafTree=Tree(leaf,None) 
            rootSet[leaf]=leafTree
        leafTree=rootSet.get(leaf)
        if leaf in nodeSet:
            nodeSet.remove(leaf)
        if tree.hasChild(leaf) == False:
            tree.add_child(leafTree)
    nodeSet.add(name)
    rootSet[name]=tree

#nodeSet.remove(0)
for node in nodeSet:
    print "final table:"
    print rootSet.get(node)
      

