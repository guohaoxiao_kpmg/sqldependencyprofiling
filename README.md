Run command:
python sqlProfiling.py yoursqlfile

Output:
table dependency of tree structure 

```
#!python

final table:
'crowle.okr_1_5_nyc_member_high_accuracy_summary'
	'crowle.okr_1_5_nyc_member_flags_clean'
		'crowle.okr_1_5_nyc_member_flags'
			'jlia11.dragon_city_nyc_postal_code_lookup'
			'crowle.okr_1_5_zcta_to_spatial_postal_codes'
				'jlia11.dragon_city_nyc_postal_code_lookup'
				'crowle.okr_1_5_spatial_join_non_zcta_postal_codes'
					'crowle.temp_external_okr_1_5_spatial_join_non_zcta_postal_codes'
			'crowle.okr_1_5_nyc_region_members_ltd'
				'member.membership_results'
			'crowle.okr_1_5_nyc_active_member_domain'
				'member.membership_results'
				'datascience_ck.mloc_x_loc_v2_upm'
			'member.member'
			'datascience_ck.mloc_x_loc_v2_upm'
		'jlia11.dragon_city_nyc_postal_code_lookup'
```